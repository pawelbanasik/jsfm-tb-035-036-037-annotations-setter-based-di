package com.pawelbanasik;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.pawelbanasik.domain.Organization;

public class DISetterApp {
	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(ConfigProperty.class);
		
		Organization organization = (Organization) context.getBean("myorg");
		
		System.out.println(organization.corporateSlogan());

		System.out.println(organization);
		
		System.out.println(organization.corporateService());
		
		((AbstractApplicationContext) context).close();
		
		
	}
}
