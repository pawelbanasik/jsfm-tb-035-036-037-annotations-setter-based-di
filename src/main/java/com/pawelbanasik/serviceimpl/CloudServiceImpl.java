package com.pawelbanasik.serviceimpl;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.pawelbanasik.service.BusinessService;

@Service("cloudService")
public class CloudServiceImpl implements BusinessService {

	public String offeringService(String companyName) {
		Random random = new Random();
		String service = "\nAs an Organization, " + companyName + " offers world class Cloud computing infrastructure."
				+ "\nThe annual income exceeds " + random.nextInt(revenue) + " dollars.";

		return service;

	}

}
