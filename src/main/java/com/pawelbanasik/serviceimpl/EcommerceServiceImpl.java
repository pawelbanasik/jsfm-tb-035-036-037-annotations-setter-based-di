package com.pawelbanasik.serviceimpl;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.pawelbanasik.service.BusinessService;

@Service("ecommerceService")
public class EcommerceServiceImpl implements BusinessService {

	public String offeringService(String companyName) {
		Random random = new Random();
		String service = "\nAs an Organization, " + companyName +
				" provides an outstanding Ecommerce platform." + 
				"\nThe annual revenue exceeds " + random.nextInt(revenue) + " dollars.";
		
		return service;
	}

}
