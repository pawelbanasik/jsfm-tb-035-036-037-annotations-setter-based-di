package com.pawelbanasik.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.pawelbanasik.service.BusinessService;

@Component("myorg")
public class Organization {

	@Value("${companyName}")
	private String companyName;
	
	@Value("${yearOfIncorporation}")
	private int yearOfIncorporation;
	
	private String postalCode;
	private String employeeCount;
	private String slogan;
	
	@Autowired
	@Qualifier("ecommerceService")
	private BusinessService businessService;
	
	
	public Organization() {
	}

	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
	}

//	public void corporateSlogan() {
//		String slogan = "We build the ultimate driving machines";
//		System.out.println(slogan);
//	}
	
	public String corporateSlogan() {
		return slogan;
	}

	// accesses the buisness service and executes the office service methods 
	// after the injection of the desired service implementation
	public String corporateService() {
		return businessService.offeringService(companyName);
	}
	
	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation
				+ ", postalCode=" + postalCode + ", employeeCount=" + employeeCount + "]";
	}

	@Autowired
	public void setEmployeeCount(@Value("${employeeCount}")String employeeCount) {
		this.employeeCount = employeeCount;
	}
	
	@Autowired
	public void setPostalCode(@Value("${postalCode}") String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Autowired
	public void setSlogan(@Value("${slogan}") String slogan) {
		this.slogan = slogan;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}
	
	
}
